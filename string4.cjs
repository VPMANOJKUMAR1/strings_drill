function titleCase(str){
    let array=[];
    for(let key in str){
        array.push(str[key]);
    }
    array= array.toString().toLowerCase().split(',');
    for (var i = 0; i < array.length; i++) {
        array[i] = array[i].charAt(0).toUpperCase() + array[i].slice(1); 
    }
    return array.join(' ');
}

//console.log(titleCase(str));
module.exports = titleCase;
